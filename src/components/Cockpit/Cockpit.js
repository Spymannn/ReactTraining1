import React from 'react';
import PropTypes from 'prop-types'

// import Person from './../Person/Person';
import Aux from './../../hoc/Aux';

const cockpit = (props) => (
    <Aux>
        {/* <Person name='Samir' age='28'> Hello you! </Person> */}
        <button onClick={props.connect}>{props.isConnected? 'Disconnect':'Connect'}</button>
        <h1> {props.isConnected ? 'Good morning Samir Hanini' : null}</h1>
        <button onClick={props.toggleTeam} disabled={!props.isConnectedToDB || !props.isConnected}>{props.isTeamShowed? 'Disable list':'Enable list'}</button>
    </Aux> 
);

cockpit.propTypes = {
    toggleTeam: PropTypes.func,
    isTeamShowed: PropTypes.bool,
    connect: PropTypes.func,
    isConnected: PropTypes.bool,
    isConnectedToDB: PropTypes.bool
}

export default cockpit;