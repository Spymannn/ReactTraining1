import React from 'react';

const showStats = ({teamStat, listStat, selectStat}) => {
    if (teamStat.length > 0 ) {
        return teamStat.map((stat, index) => {
            return <p key={index}>{stat.name} | {stat.coef}</p>
        });
    }
    else {
        return listStat.map((stat, index) => {
        return( <button key={index} onClick={() => selectStat(stat)}>{stat.name}</button> )
        });
    }
}


export default showStats;