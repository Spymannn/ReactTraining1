import React, {Component} from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import Aux from './../../../hoc/Aux';

class AddStat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            coef: 0
        }
        
    }

    changeCoefHandler(e) {
        e.preventDefault();
        console.log(e.target.value);
    }

    componentDidMount()  {
    }
    
    render() {
        return (
            <Aux>
                <h3> Add the coef for your stat: {this.props.stat.name}</h3>
                <input
                    type='number' 
                    value={this.state.coef}
                    onChange={this.changeCoefHandler}
                />
                <button onClick={() => this.addStat(this.props.stat, this.state.coef)}> Add stat </button>
            </Aux> 
        );
    }

}


AddStat.propTypes = {
    coef: PropTypes.number,
    addStat: PropTypes.func,
    statSelected: PropTypes.bool,
    stat: PropTypes.object
}

export default AddStat;