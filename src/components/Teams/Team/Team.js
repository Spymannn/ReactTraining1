import React, {Component}from 'react'
import PropTypes from 'prop-types';
import axios from 'axios';

import './Team.css';

// Component 
import ShowStats from './../../Stats/showStats/showStats';
import AddStat from './../../Stats/addStat/addStat';



// Context 
import { ConnectedContext } from './../../../containers/App';

class Team extends Component {
    constructor (props) {
        super(props);
        this.inputEl = React.createRef();
        this.state = {
            teamStat: [],
            statsName: [],
            showStats: false,
            statToAdd: {
                id: null,
                name: null
            }
        }
    }

    componentDidMount()  {
        if (this.props.position === 0) {
            this.inputEl.current.focus();
        }

        if (this.props.idTeam === 0) {
            this.getStatsTeamHandler(this.props.idTeam);
        }
    }

    getStatsTeamHandler = (teamId) => {
        axios.get('http://localhost:5000/teamstats?idTeam='+teamId)
            .then( success => {
                    success.data.forEach( stat => {
                        this.getStatsName(stat);
                    });
                }, error => {
                    console.log(error);
                }
            );
    }

    getStatsName = (stats) => {
        axios.get('http://localhost:5000/stats/'+stats.id)
            .then(
                success => {
                    const teamStatLine = {...stats, name: success.data.name};
                    const statArrayComplete = [...this.state.teamStat, teamStatLine];
                    this.setState({teamStat: statArrayComplete});
                }, error => {
                    console.log('error: ', error);
                }
            )
    }   
    
   

    updateShowStats() {
        this.setState({showStats: !this.state.showStats});
    }

    selectStat(stat) {
        console.log('stat : ', stat);
        this.setState({statToAdd: stat});
    }

    addStat() {

    }

    render() {     
        const { teamStat } = this.state; // deconstruction JS ES6 

        return  (
            <div className='Team' >
                <p>Team: {this.props.name} | city: {this.props.city}</p>    <a onClick={this.props.action}>X</a>
                <button onClick={() => this.updateShowStats()}> {this.state.showStats ? 'Hide statistics': 'Show statistics'} </button>
                <div>
                    {
                        (this.state.showStats ? <ShowStats teamStat={teamStat} listStat={this.props.listStat} selectStat={this.selectStat.bind(this)}/> : null)
                    }
                    {
                        (this.state.teamStat.length === 0 && this.state.showStats && this.state.statToAdd ? 
                            <AddStat stat={this.state.statToAdd} /> : 
                            null)
                    }
                </div>
                <ConnectedContext.Consumer>
                    {
                        (auth) => {
                            return (<input 
                                    ref = {this.inputEl}
                                    type='text' 
                                    onChange={this.props.nameChanged} 
                                    value={this.props.name}
                                    disabled={!auth}
                                />);
                        }
                    }
                </ConnectedContext.Consumer>   
            </div>
        )}
}

Team.propTypes = {
    name: PropTypes.string,
    city: PropTypes.string,
    action: PropTypes.func,
    nameChanged: PropTypes.func,
    position: PropTypes.number,
    idTeam: PropTypes.number,
    listStat: PropTypes.array
}

// const team = (props) => {
//     return (
//         <div className='Team' >
//             <p>Team: {props.name} | city: {props.city}</p>    <a onClick={props.action}>X</a>
//             <input type='text' onChange={props.nameChanged} value={props.name}/>
//         </div>
//     );
// }

export default Team;