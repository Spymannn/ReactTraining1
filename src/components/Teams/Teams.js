import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Team from './Team/Team';
import axios from 'axios';


class Teams extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stats: []
        }
    }

    componentDidMount() {
        if (this.state.stats.length === 0) {
            this.getListStat();
        } 
    }

    getListStat() {
        axios.get('http://localhost:5000/stats')
            .then(
                success => {
                    console.log('success: ', success);
                    this.setState({stats: success.data});
                }, error => {
                    console.log('error: ', error);
                }
            )
    }


    render() {
        if (this.state.stats.length > 0)  {
            return this.props.teams.map((team, index) => {
                return <Team
                    position = {index}
                    city={team.city} 
                    name={team.name} 
                    action={() => this.props.action(index)} 
                    key={team.id}
                    idTeam={team.id}
                    nameChanged={(e) => this.props.nameChanged(e, team.id)}
                    listStat={this.state.stats}
                    />
            });
        } else {
            return <p>Loading...</p>
        }
    }
}

Teams.propTypes = {
    teams: PropTypes.array,
    action: PropTypes.func,
    nameChanged: PropTypes.func,
}

// const teams = (props) => 
//     props.teams.map((team, index) => {
//         return <Team 
//             city={team.city} 
//             name={team.name} 
//             action={() => props.action(index)} 
//             key={team.id}
//             nameChanged={(e) => props.nameChanged(e, team.id)}
//             />
//       });

export default Teams;