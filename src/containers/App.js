import React, { Component } from 'react';
import './App.css';

import axios from 'axios';
// Components
import Teams from './../components/Teams/Teams';
import Cockpit from './../components/Cockpit/Cockpit';

// Contexts 
export const ConnectedContext = React.createContext(false);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teams: [],
      showTeams: false,
      isConnected: false,
      isConnectedToDB: true
    }
  }

  componentDidMount() {
    console.log('component did mount');
    axios.get('http://localhost:5000/teams')
      .then( success => {
        this.setState({teams: success.data});
        this.setState({isConnectedToDB: true});
        }, error => {
          this.setState({isConnectedToDB: false});
          console.log(error);
        }
      );
  }

  
  

  connect = () => {
    this.setState((prevState, props) => {
        return {
          isConnected: !prevState.isConnected
        }
      }
    );
    console.log('connexion: ', this.state.isConnected);
  }

  nameChangedHandler = (event, id) => {
    event.preventDefault();
    const teamIndex = this.state.teams.findIndex(t => t.id === id );
    const team = {...this.state.teams[teamIndex]};
    team.name = event.target.value;

    const teams = [...this.state.teams];
    teams[teamIndex] = team;

    this.setState({teams: teams});
  } 

  deleteTeamHandler = (indexTeam) => {
    const teams = [...this.state.teams];
    teams.splice(indexTeam, 1);
    this.setState({teams: teams});
  }

  toggleTeamsHandler = () => {
    this.setState({showTeams: !this.state.showTeams});
  }


  componentDidUpdate() {
    // console.log('Component did update');    
  }
 
  render() {
    let teams = null;
    if (this.state.showTeams) {
      teams = 
          <Teams teams={this.state.teams} 
            action={this.deleteTeamHandler}
            nameChanged={this.nameChangedHandler}
          />
    }

    let noConnectedToDB = <p>No connexion with the database </p>;

    return (
      <div className="App" id = "teamsTotal"
      >
        <Cockpit 
          isTeamShowed={this.state.showTeams}
          toggleTeam={this.toggleTeamsHandler}
          connect = {this.connect}
          isConnected = {this.state.isConnected}
          isConnectedToDB = {this.state.isConnectedToDB}
        />
        <ConnectedContext.Provider value={this.state.isConnected}> { (this.state.isConnectedToDB ? teams : noConnectedToDB )} </ConnectedContext.Provider> 
      </div>
    );
  }
}

export default App;
